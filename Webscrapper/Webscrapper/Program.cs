﻿using HtmlAgilityPack;
using System;
using System.Linq;
using System.Net.Http;

namespace Webscrapper
{
    class Program
    {
        static void Main(string[] args)
        {
            //Main User Instructions
            Console.WriteLine("Hello, This is Khurram Nazir's SEO tool.");
            Console.WriteLine("This tool is going to see if www.infotrack.co.uk appears in the top 100 searches");
            Console.WriteLine("for the term, 'land registry search'.");
            Console.WriteLine();
            Console.WriteLine("Enter your search criteria");
            var searchText = Console.ReadLine();
            Console.WriteLine("Enter your website url");
            var websiteUrl = Console.ReadLine();
            var searchArray = searchText.Split(new char[0]);
            searchText = string.Join('+', searchArray);
          
            var url = string.Format("https://www.google.co.uk/search?q={0}&num=100", searchText);
                
            //Function for retrieving and checking the data
            getHtmlAsync(url, websiteUrl);
            //Reading a line to give the user option to exit the program.
            Console.ReadLine();
        }

        private static async void getHtmlAsync(string url, string websiteUrl)
        {            
            var httpClient = new HttpClient();
            var html = await httpClient.GetStringAsync(url);
            //Getting the URL, making a new HTTPClient and getting the html generated through the url of this page.

            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(html);
            //Generating a new HTMLDocument and loading html on the document.

            var webListHtml = htmlDocument.DocumentNode.Descendants("div")
                .Where(node => node.GetAttributeValue("class", "")
                .Equals("ZINbbc xpd O9g5cc uUPGi")).ToList();
            //Looking through the descendants of the html document and getting all of the 'divs with class = ZINbbc xpd O9g5cc uUPGi' 
            // Getting ZINbbc xpd O9g5cc uUPGi is especially difficult as most text editors and Google Chrome's Inspect Element
            //were showing a different class name than this.

            var COUNT = 0; // Count to see the rank of the website
            var foundWebsite = false; //Check to see if the website has been found.

            foreach (var webListItem in webListHtml)
            {
                //Checking each item from the descendants that matched 'divs with class = ZINbbc xpd O9g5cc uUPGi'
                if (webListItem.InnerHtml.Contains(websiteUrl))
                {
                    //Checking if each item in the list of descendents includes www.infotrack.co.uk 
                    Console.WriteLine(string.Format("{0} has been Found.", websiteUrl));
                    Console.WriteLine("Your website is ranked at: " + COUNT);
                    //Print success and show the position website found.
                    foundWebsite = true;
                    //Website found, so check accepted and variable changed.
                }
                COUNT++; // Add the count value for the rank of the website.
            }

            if (foundWebsite == false)
            {
                //Check if website was not found and display a not found message to the user.
                Console.WriteLine("Sorry, your website, www.infotrack.co.uk, didn't appear in the top 100 results.");
            }
            Console.WriteLine();


        }
    }
}
